/**
 * 
 */
package uc.epam.helloworld;

import uc.epam.helloworld.staff.NewClass1;

/**
 * @author VIK
 *
 */
public class Runner {

    /**
     * @param args
     */
    public static void main(String[] args) {
	System.out.println("Hello World!!!\n");

	NewClass1 class1 = new NewClass1();
	NewClass2 class2 = new NewClass2();
	NewClass3 class3 = new NewClass3();

	System.out.println(class1.toString());
	System.out.println(class2.toString());
	System.out.println(class3.toString());

	System.out.println();

	for (int i = 0; i < args.length; i++) {
	    System.out.println((i + 1) + " параметр args[" + i + "] = " + args[i]);
	}

    }

}
